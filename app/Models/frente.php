<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class frente extends Model
{
    use HasFactory;
    protected $primaryKey = 'idfrente';
    protected $table = 'frentes'; 
    protected $fillable = [
        'nombre',
        'sigla',
        'idfuerza',
        'descripcion',
        'propuestas',
        'rutafoto',
        'consolidado',
        'activo',
        'isnulo'
    ];
    public function candidatos(){
        return $this->hasMany(Candidato::class,'idfrente')->where('activo','1');
    }
}
