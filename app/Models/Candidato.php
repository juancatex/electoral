<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Candidato extends Model
{
    use HasFactory;
    protected $primaryKey = 'idcandidato';
    protected $table = 'candidatos'; 
    public function data(){
        return $this->belongsTo(frente::class);
    }
    public function requisitos(){
        return $this->hasMany(CandidatoRequisitos::class,'idcandidato')->where('activo','1');
    }
}
