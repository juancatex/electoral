<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Frentes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('frentes')->insert(['nombre'=>'Voto nulo Ejercito','sigla'=>'N-ejer','idfuerza'=>3,'descripcion'=>'Voto nulo por Ejercito','propuestas'=>'Voto nulo','rutafoto'=>'nulo.jpg','consolidado'=>1,'isnulo'=>1]);   
        DB::table('frentes')->insert(['nombre'=>'Voto nulo Aerea','sigla'=>'N-aer','idfuerza'=>4,'descripcion'=>'Voto nulo por Aerea','propuestas'=>'Voto nulo','rutafoto'=>'nulo.jpg','consolidado'=>1,'isnulo'=>1]);   
        DB::table('frentes')->insert(['nombre'=>'Voto nulo Armada','sigla'=>'N-arm','idfuerza'=>5,'descripcion'=>'Voto nulo por Armada','propuestas'=>'Voto nulo','rutafoto'=>'nulo.jpg','consolidado'=>1,'isnulo'=>1]);   
    }
}
