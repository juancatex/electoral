<!DOCTYPE html>
<html>
<head>
<style>
     
    body{
            font-size: 10px;
    }
    .text-align-center { 
        width: 100%;    
        line-height: 100px;
        text-align: center;
        vertical-align: middle;
        }
       .tablet   {
  border-bottom: 1px solid gray;  
}
.center { 
         
	line-height:auto;
	padding: 20px 0 20px 0;
	text-align:center; 
}
.center img {
	vertical-align:middle;
}

 
.imgtt {
   margin: 15px 0 0 0;
  width: auto;
  height: 100%;
}
.table td, .table th {
    padding: 0.25rem !important;  
    vertical-align: middle;
    border-top: 1px solid black !important;
}
  </style> 
    <title>reporte</title> 
    <link type="text/css" href="css/bootstrap.min.css" rel="stylesheet" /> 
</head>
<body>  
        <main> 
        
       <table class="tablet" style="width: 100%;  ">
                <tr>
                                <td class="center"> <img src="{{$foto}}" width="100px"> </td>
                                <td style="text-align:center;"> <p style="line-height : 18px;"> ASOCIACION NACIONAL DE SUBOFICIALES Y SARGENTOS DE LAS FUERZAS ARMADAS DEL ESTADO</p><h4>LISTA DE DOCUMENTOS PRESENTADOS POR FRENTE</h4></td>
                                <td class="center"> <img src="{{$foto}}" width="100px" > </td>
                </tr>
       </table>
   

       @foreach ($frentes as $key => $frente) 
<label style="margin-top:5px; ">{{strtoupper($frente['nombre'])}}</label> 
    <table class="table table-bordered" style="width: 100%;  margin-top:5px;font-size: 10px; ">
        <tr class="table-secondary">
        <th style="text-align:center;">No.</th>
        <th style="text-align:center;">Candidato</th>
            @foreach ($reqs as $key => $re)   
                    <th style="text-align:center;font-size: 9px;">{{ $re}}</th>
            @endforeach 
        </tr> 
            @foreach ($frente['candidatos'] as $key => $socio)  
                <tr>   
                    <td style="text-align:center;font-size: 9px;"> {{ $key+1 }} </td>
                    <td > {{ $socio['socio'] }} </td>  
                        @foreach ($reqs as $keyreq => $re)    
                                    @if (in_array($keyreq, $socio['req']))
                                      <td style="text-align:center;font-size: 9px;" >Si</td>
                                    @else
                                      <td style="text-align:center;font-size: 9px;" >No</td>
                                    @endif
                        @endforeach 
                </tr>  
            @endforeach 
    </table>
    @endforeach 
    <table class="" style="width: 100%;  margin-top:250px; ">
        <tr> 
                    <td colspan="3" style="text-align:center;font-size:11px ">
                    SOM. DESN. Wilson Constantino Almanza Angulo<br>
                    VICEPRESIDENTE COMITE ELECTORAL <br> NACIONAL "ASCINALSS"
                    </td>  
        </tr> 

   <tr style="margin-top:100px;">
   <td style="text-align:center;font-size:11px;padding-top:100px;  ">
                Sof. 1ro. DEPSS. Gabriel Huanca Chura<br>
                SECRETARIO DE ACTAS COMITE<br> ELECTORAL"ASCINALSS"
     </td> 
                    <td> 
                    </td> 
    <td style="text-align:center;font-size:11px;padding-top:100px;  ">
    Sof. 1ro. DEPSS. Juan Colque Quispe<br>
    1ER. VOCAL<br> REPRESENTANTE DEL EJÉRCITO
    </td> 
   </tr> 
   
   <tr >
   <td style="text-align:center;font-size:11px;padding-top:100px; ">
   Sof. My. Javier Mamani Churata<br>
   2DO. VOCAL<br> REPRESENTANTE DE LA FUERZA AEREA
     </td> 
                    <td> 
                    </td> 
    <td style="text-align:center;font-size:11px;padding-top:100px; ">
    SOM. DESN. Eliseo Quispe Cachaca<br>
    3ER. VOCAL<br> REPRESENTANTE DE LA ARMADA BOLIVIANA
    </td> 
   </tr> 
</table>
   </main>
      
</body>
</html>

